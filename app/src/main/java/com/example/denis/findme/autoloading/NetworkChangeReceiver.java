package com.example.denis.findme.autoloading;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.denis.findme.sendinfo.SendCoordinates;


/**
 * Class used for check set on/off values of internet connection. We create SendCoordinates object in TimerManager class
 * and with static access work with them (used for avoid several stating timer).
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    SendCoordinates send;
    boolean connection;
    public static int networkChange = 0;

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        // Checking internet connection after change state of any network service
        if ((wifi != null && wifi.isConnectedOrConnecting()) || (mobile != null && mobile.isConnectedOrConnecting())){
            connection = true;
        }

        send = new SendCoordinates(context);
        new TimerManager(send);
        if (networkChange == 0){
            if (connection) {
                if (TimerManager.mySend != null){
                    send = new SendCoordinates(context);
                    new TimerManager(send, 1);
                }
                TimerManager.start();
                Log.d("Network Available ", "YES");
                networkChange++;
            } else {
                TimerManager.stop();
                Log.d("Network Available ", "NO");
                networkChange++;
            }
        } else {
            networkChange = 0;
        }

    }

}
