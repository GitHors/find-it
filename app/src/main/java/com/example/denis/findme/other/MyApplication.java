package com.example.denis.findme.other;

import android.app.Application;
import android.content.Context;

/**
 * Created by denis on 22.5.16.
 */
public class MyApplication extends Application {

    private static Context context;

    public void onCreate(){
        super.onCreate();
        MyApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApplication.context;
    }
}
