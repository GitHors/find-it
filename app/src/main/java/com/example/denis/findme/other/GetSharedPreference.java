package com.example.denis.findme.other;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.denis.findme.MainActivity;

/**
 * Class used for create sharedPerefernce file and get it's filed.
 * We create this class because of error in getting prefernce settins in class which dont't extends from Activity class.
 */
public class GetSharedPreference extends Activity{

    public SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE);

    /**
     * Method for getting sharedPrefernce.
     * @return
     */
    public SharedPreferences getPreference(){
        sharedPreferences = getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE);
        return sharedPreferences;
    }
}
