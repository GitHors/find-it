package com.example.denis.findme;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.denis.findme.database.GetInfo;
import com.example.denis.findme.database.UpdateInfo;
import com.example.denis.findme.other.Constants;


public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    // Names for keys in sharedPreference
    private static final String FIRST_SEND_AFTER_TURN_ON_NETWORK_CONNECTION = "connection";
    private static final String SEND_COORDINATEN_KEY_NAME = "sendCoord";
    private static final String PROGRAM_STATE_KEY_NAME = "programmState";
    private static final String SHOW_BLOCK_KEY_NAME = "showBlock";
    private static final String SHOW_INFO_KEY_NAME = "refreshInfo";
    private static final String UPDATE_TIME_KEY_NAME = "updateTime";
    private static final String FIRST_START_KEY_NAME = "start";

    // Default values (on/off) of state settings in sharedPreference
    private static final String ON_PREFERENCE_STATUS = "1";
    private static final String OFF_PREFERENCE_STATUS = "0";

    // Name of settings file
    public static final String APP_SETTINGS = "coordinates";
    public static SharedPreferences settings;

    // ImageButtons for setting in main app screen
    public ImageButton mSendCoordButton, mShowBlock, mRefreshInfo, mButton4, mShowTutorial, mProgrammState;
    TextView mSendCoordText;
    TextView mShowBlockText;
    TextView mRefreshInfoText;
    TextView mProgrammStateText;
    TextView mUpdateTimeText;

    SeekBar seekbar;
    CheckPreference preference;

    /**
     * In this method we create a lot of sharepPrefernce variable to check a lot of settings.
     * One of them is variable for first start sending coordinat information after turning on network connection.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekbar = (SeekBar) findViewById(R.id.updateTimeSeek);
        seekbar.setOnSeekBarChangeListener(this);

        Constants.FIRST_CONNECTION = 1;

        settings = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);
        preference = new CheckPreference();
        // Check app for first start (if true initialize all preference in "off" condition)
        if (!settings.getString(FIRST_START_KEY_NAME, "").equals("1")) {
            seekbar.setProgress(0);

            // Prefernce for check first start reading information after turn on network connection
            preference.setPreferenceValue(settings, FIRST_SEND_AFTER_TURN_ON_NETWORK_CONNECTION, ON_PREFERENCE_STATUS);

            preference.setPreferenceValue(settings, FIRST_START_KEY_NAME, ON_PREFERENCE_STATUS);
            preference.setPreferenceValue(settings, SEND_COORDINATEN_KEY_NAME, OFF_PREFERENCE_STATUS);
            preference.setPreferenceValue(settings, SHOW_BLOCK_KEY_NAME, OFF_PREFERENCE_STATUS);
            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, OFF_PREFERENCE_STATUS);
            preference.setPreferenceValue(settings, PROGRAM_STATE_KEY_NAME, OFF_PREFERENCE_STATUS);

            preference.setPreferenceValue(settings, UPDATE_TIME_KEY_NAME, String.valueOf(seekbar.getProgress()));
        }

        mSendCoordButton = (ImageButton) findViewById(R.id.sendCoordButton);
        mShowBlock = (ImageButton) findViewById(R.id.showBlock);
        mRefreshInfo = (ImageButton) findViewById(R.id.refreshInfo);
        mShowTutorial = (ImageButton) findViewById(R.id.showTutorial);
        mProgrammState = (ImageButton) findViewById(R.id.programmState);

        mSendCoordText = (TextView) findViewById(R.id.sendCoordTextView);
        mShowBlockText = (TextView) findViewById(R.id.showBlockText);
        mRefreshInfoText = (TextView) findViewById(R.id.refreshInfoText);
        mProgrammStateText = (TextView) findViewById(R.id.programmStateText);
        mUpdateTimeText = (TextView) findViewById(R.id.updateTimeText);

        preference.setTextAboutPreferState(mSendCoordText, settings, SEND_COORDINATEN_KEY_NAME);
        preference.setTextAboutPreferState(mShowBlockText, settings, SHOW_BLOCK_KEY_NAME);
        preference.setTextAboutRefreshState(mRefreshInfoText, settings, SHOW_INFO_KEY_NAME);
        preference.setTextAboutPreferState(mProgrammStateText, settings, PROGRAM_STATE_KEY_NAME);

        mUpdateTimeText.setText(settings.getString(UPDATE_TIME_KEY_NAME, ""));
        seekbar.setProgress(Integer.valueOf(settings.getString(UPDATE_TIME_KEY_NAME, "")));

    }

    /**
     * Method of SeekBar.OnSeekBarChangeListener interface.
     *
     * @param seekBar
     * @param progress
     * @param fromUser
     */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        mUpdateTimeText.setText(String.valueOf(seekBar.getProgress()));
        preference.setPreferenceValue(settings, UPDATE_TIME_KEY_NAME, String.valueOf(seekBar.getProgress()));
//        mRefreshInfoText.setText(Constants.REFRESH_BUTTON);
    }

    /**
     * Method of SeekBar.OnSeekBarChangeListener interface.
     *
     * @param seekBar
     */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    /**
     * Method of SeekBar.OnSeekBarChangeListener interface.
     *
     * @param seekBar
     */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mUpdateTimeText.setText(String.valueOf(seekBar.getProgress()));
        preference.setPreferenceValue(settings, UPDATE_TIME_KEY_NAME, String.valueOf(seekBar.getProgress()));
        mRefreshInfoText.setText(Constants.REFRESH_BUTTON);
        preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, ON_PREFERENCE_STATUS);
    }

    /**
     * Geolocation coordinates send on/off.
     *
     * @param view
     */
    public void OnClickSendCoordButton(View view) {
        if (settings.getString(SEND_COORDINATEN_KEY_NAME, "")
                .equals(OFF_PREFERENCE_STATUS)) {
            preference.setPreferenceValue(settings, SEND_COORDINATEN_KEY_NAME, ON_PREFERENCE_STATUS);
            mSendCoordText.setText("On");
            mRefreshInfoText.setText(Constants.REFRESH_BUTTON);
            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, ON_PREFERENCE_STATUS);
        } else {
            mSendCoordText.setText("Off");
            preference.setPreferenceValue(settings, SEND_COORDINATEN_KEY_NAME, OFF_PREFERENCE_STATUS);
            mRefreshInfoText.setText(Constants.REFRESH_BUTTON);
            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, ON_PREFERENCE_STATUS);
        }
    }

    /**
     * @param view
     */
    public void OnClickShowBlock(View view) {
        if (settings.getString(SHOW_BLOCK_KEY_NAME, "")
                .equals(OFF_PREFERENCE_STATUS)) {
            preference.setPreferenceValue(settings, SHOW_BLOCK_KEY_NAME, ON_PREFERENCE_STATUS);
            mShowBlockText.setText("On");
            mRefreshInfoText.setText(Constants.REFRESH_BUTTON);
            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, ON_PREFERENCE_STATUS);
        } else {
            preference.setPreferenceValue(settings, SHOW_BLOCK_KEY_NAME, OFF_PREFERENCE_STATUS);
            mShowBlockText.setText("Off");
            mRefreshInfoText.setText(Constants.REFRESH_BUTTON);
            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, ON_PREFERENCE_STATUS);
        }
    }

    /**
     * @param view
     */
    public void OnClickShowInfo(View view) {
//        if (settings.getString(SHOW_INFO_KEY_NAME, "")
//                .equals(OFF_PREFERENCE_STATUS)){
//            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, ON_PREFERENCE_STATUS);
//            mRefreshInfoText.setText("On");
//        } else {
//            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, OFF_PREFERENCE_STATUS);
//            mRefreshInfoText.setText("Off");
//        }
        if (isInternetConnectionExist()) {
            mRefreshInfoText.setText(Constants.REFRESH_BUTTON_SIMPLE_STATE);
            preference.setPreferenceValue(settings, SHOW_INFO_KEY_NAME, OFF_PREFERENCE_STATUS);
//            new UpdateInfo("234234", "253542", "1", "1", "1", "1", "1").execute();
            new GetInfo("234234", "253542").execute();
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    Constants.ERROR_INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            toast.show();
        }


    }

    /**
     * Method for showing activity with tutorial program.
     *
     * @param view View value.
     */
    public void OnClickShowTutorial(View view) {
//        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
//        startActivity(intent);
    }

    /**
     * Method for turn on and turn off all functions of this app.
     *
     * @param view
     */
    public void OnClickProgrammState(View view) {
        if (settings.getString(PROGRAM_STATE_KEY_NAME, "")
                .equals(OFF_PREFERENCE_STATUS)) {
            preference.setPreferenceValue(settings, PROGRAM_STATE_KEY_NAME, ON_PREFERENCE_STATUS);
            mProgrammStateText.setText("On");
            mProgrammState.setImageResource(R.mipmap.works);
        } else {
            preference.setPreferenceValue(settings, PROGRAM_STATE_KEY_NAME, OFF_PREFERENCE_STATUS);
            mProgrammStateText.setText("Off");
            mProgrammState.setImageResource(R.mipmap.close);
        }
    }

    /**
     * Method for check internet connection.
     *
     * @return true if phome has internet connection, false if not.
     */
    private boolean isInternetConnectionExist() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }
}
