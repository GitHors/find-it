package com.example.denis.findme.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import com.example.denis.findme.MainActivity;
import com.example.denis.findme.other.Constants;


/**
 * Class used for connect with database. Check user in database (if user with such name exist put error into key).
 */
public class UpdateInfo extends AsyncTask<String, String, String> {
    /**
     * Async Task for registration new user in database.
     **/

        // 1 - if user with such login exist, 0 - if not
        public static int error = 0;
        // 1 - if we have answer of server , 0 - if we haven't
        public static int answerState = 0;

        JSONParser jsonParser = new JSONParser();

        private String login;
        private String password;
        private String sendCoordinates;
        private String blockScreen;
        private String latitude;
        private String longtitude;
        private String data;


        public UpdateInfo(String login, String password, String sendCoordinates, String blockScreen, String latitude,
                          String longtitude, String data){

            error = 0;
            answerState = 0;

            this.login = login;
            this.password = password;
            this.sendCoordinates = sendCoordinates;
            this.blockScreen = blockScreen;
            this.latitude = latitude;
            this.longtitude = longtitude;
            this.data = data;
        }

        /**
         * Update info about user in database.
         **/
        protected String doInBackground(String[] args) {

            try{
                // Filling parameters
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(Constants.DATABASE_ID_USER_FIELD, login));
                params.add(new BasicNameValuePair(Constants.DATABASE_PASSWORD_USER_FIELD, password));
                params.add(new BasicNameValuePair(Constants.DATABASE_SEND_COORD_FIELD, sendCoordinates));
                params.add(new BasicNameValuePair(Constants.DATABASE_BLOCK_FIELD, blockScreen));
                params.add(new BasicNameValuePair(Constants.DATABASE_LATITUDE_FIELD, latitude));
                params.add(new BasicNameValuePair(Constants.DATABASE_LONGTITUDE_FIELD, longtitude));
                params.add(new BasicNameValuePair(Constants.DATABASE_DATA_FIELD, data));

                // Giving JSON object after execute php file on the server
                JSONObject json = jsonParser.makeHttpRequest(Constants.UPDATE_INFO_PHP_FILE_URL, "POST", params);
                Log.d("Answer from server", json.toString());

                try {
                    int success = json.getInt("success");
                    Log.d("Answer from server", json.toString());
                    if (success == 1) {
                        // Put 1 at the key as a flag of finish process for LoginActivity.java
                        answerState = 1;
//                        finish();
                    } else if (success == 0){
                        // Put error into key that means that user with such name already exist
                        error = 1;
                        // Put 1 at the key as a flag of finish process for LoginActivity.java
                        answerState = 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e){
                System.out.println("Exp=" + e);
            }
            answerState = 1;
            return null;
        }
    }
