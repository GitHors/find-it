package com.example.denis.findme.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import com.example.denis.findme.MainActivity;
import com.example.denis.findme.other.Constants;


/**
 */
public class GetInfo extends AsyncTask<String, String, String> {

    // 1 - if user with such login exist, 0 - if not
    public static int error = 0;
    // 1 - if we have answer of server , 0 - if we haven't
    public static int answerState = 0;

    JSONParser jsonParser = new JSONParser();

    private String login;
    private String password;
    public static String sendCoordinates;
    public String blockScreen;
    public String latitude;
    public String longtitude;
    public String data;


    public GetInfo(String login, String password){
        error = 0;
        answerState = 0;
        this.login = login;
        this.password = password;
    }

    /**
     * Update info about user in database.
     **/
    protected String doInBackground(String[] args) {

        try {
            // Filling parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(Constants.DATABASE_ID_USER_FIELD, login));
            params.add(new BasicNameValuePair(Constants.DATABASE_PASSWORD_USER_FIELD, password));

            // Giving JSON object after execute php file on the server
            JSONObject json = jsonParser.makeHttpRequest(Constants.GET_INFO_PHP_FILE_URL, "POST", params);
            Log.d("Answer from server", json.toString());

            try {
                int success = json.getInt("success");
                Log.d("Answer from server", json.toString());
                if (success == 1) {
                    Log.d("Check 2", "...");

                    // Put 1 at the key as a flag of finish process for LoginActivity.java
                    sendCoordinates = json.getString(Constants.DATABASE_SEND_COORD_FIELD);
                    blockScreen = json.getString(Constants.DATABASE_BLOCK_FIELD);
//                    latitude = json.getString(Constants.DATABASE_LATITUDE_FIELD);
//                    longtitude = json.getString(Constants.DATABASE_LONGTITUDE_FIELD);
//                    data = json.getString(Constants.DATABASE_DATA_FIELD);
                    answerState = 1;
//                        finish();
                } else if (success == 0) {
                    Log.d("Check 3", "Err");
                    // Put error into key that means that user with such name already exist
                    error = 1;
                    // Put 1 at the key as a flag of finish process for LoginActivity.java
                    answerState = 1;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            System.out.println("Exp=" + e);
        }
        answerState = 1;
        return null;
    }
}
