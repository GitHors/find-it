package com.example.denis.findme.login;

/**
 * Class used for check password.
 */
public final class CheckPassword {

    private String password;
    private int errorNumber = 0;
    // Length parameters of password
    private static final int MIN_PASSWORD_LENGTH = 4;
    private static final int MAX_PASSWORD_LENGTH = 15;
    // Errors
    private static final int MIN_LENGHT_ERROR = 1;
    private static final int MAX_LENGHT_ERROR = 2;
    private static final int NUMBERS_NOT_EXISTING_ERROR = 3;
    private static final int RUSSIAN_LETTERS_ERROR = 4;

    /**
     * Constructor.
     * @param password
     * String value.
     */
    public CheckPassword(String password){
        this.password = password;
    }

    /**
     * Method for all situations check.
     * if something wrong with user login set key in error number field.
     */
    public void mainCheck(){
        checkLength();
        if (errorNumber == 0){
            checkLanguage();
        }
        if (errorNumber == 0){
            checkNumberExist();
        }
    }

    /**
     * Method for checking min length for login.
     * if length less then MIN_PASSWORD_LENGTH, set "1" in errorNumber field (it is key for checking
     * state in future). if length more then MAX_PASSWORD_LENGTH, set "2" in errorNumber field
     *
     */
    private void checkLength(){
        if (password.length() < MIN_PASSWORD_LENGTH){
            errorNumber = MIN_LENGHT_ERROR;
        } else if (password.length() > MAX_PASSWORD_LENGTH){
            errorNumber = MAX_LENGHT_ERROR;
        }
    }

    /**
     * Method for checking password to exist numbers.
     * Password must contains numbers.
     */
    private void checkNumberExist(){
        String passwordCopy = password.toLowerCase();
        // Key for checking russian letters existing
        int numbersExistKey = 0;
        char passwordArray[] = passwordCopy.toCharArray();
        for (int i = 0; i < passwordCopy.length(); i++){
            if (Character.isDigit(passwordArray[i])){
                numbersExistKey = 1;
                break;
            }
        }
        if (numbersExistKey == 0){
            errorNumber = NUMBERS_NOT_EXISTING_ERROR;
        }
    }

    /**
     * Method for checking language of login. We should set "4" in in errorNumber field (it is key for checking
     * state in future) if login contains russian symbols (it is wrong for database).
     */
    private void checkLanguage(){
        final String russianAlphabet = "ёйцукенгшщзхъфывапролджэячсмитьбю";
        String passwordCopy = password.toLowerCase();
        // Key for checking russian letters existing
        int russianLettestKey = 0;
        char loginArray[] = passwordCopy.toCharArray();
        for (int i = 0; i < passwordCopy.length(); i++){
            if (russianAlphabet.indexOf(loginArray[i]) != -1){
                russianLettestKey = 1;
                break;
            }
        }
        if (russianLettestKey != 0){
            errorNumber = RUSSIAN_LETTERS_ERROR;
        }
    }

    /**
     * Method for getting error number.
     * "1" is error in length (less then min length).
     * "2" is error in length (more then max length).
     * "3" is error in russian symbols.
     * @return
     * int value.
     */
    public int getErrorNumber(){
        return errorNumber;
    }


}
