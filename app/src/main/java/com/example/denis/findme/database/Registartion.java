package com.example.denis.findme.database;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import com.example.denis.findme.other.Constants;


/**
 * Class used for connect with database. Check user in database (if user with such name exist put error into key).
 */
public class Registartion extends Activity {

    private String login;
    private String password;

    // 1 - if user with such login exist, 0 - if not
    public static int error = 0;
    // 1 - if we have answer of server , 0 - if we haven't
    public static int answerState = 0;

    JSONParser jsonParser = new JSONParser();

    /**
     * Constructor.
     */
    public Registartion(final String login, final String password){
        error = 0;
        answerState = 0;
        this.login = login;
        this.password = password;

        new CreateNewUser().execute();
    }


    /**
     * Async Task for registration new user in database.
     **/
    class CreateNewUser extends AsyncTask<String, String, String> {

        /**
         * Create user in database.
         **/
        protected String doInBackground(String[] args) {

            try{
                // Filling parameters
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair(Constants.DATABASE_ID_USER_FIELD, login));
                params.add(new BasicNameValuePair(Constants.DATABASE_PASSWORD_USER_FIELD, password));
                params.add(new BasicNameValuePair(Constants.DATABASE_SEND_COORD_FIELD, Constants.DATABASE_DEFAULT_VALUE));
                params.add(new BasicNameValuePair(Constants.DATABASE_BLOCK_FIELD, Constants.DATABASE_DEFAULT_VALUE));
                params.add(new BasicNameValuePair(Constants.DATABASE_LATITUDE_FIELD, Constants.DATABASE_DEFAULT_VALUE));
                params.add(new BasicNameValuePair(Constants.DATABASE_LONGTITUDE_FIELD, Constants.DATABASE_DEFAULT_VALUE));
                params.add(new BasicNameValuePair(Constants.DATABASE_DATA_FIELD, Constants.DATABASE_DEFAULT_VALUE));

                // Giving JSON object after execute php file on the server
                JSONObject json = jsonParser.makeHttpRequest(Constants.REGISTRATION_PHP_FILE_URL, "POST", params);
                Log.d("Answer from server", json.toString());

                try {
                    int success = json.getInt("success");

                    if (success == 1) {
                        // Put 1 at the key as a flag of finish process for LoginActivity.java
                        answerState = 1;
                        finish();
                    } else if (success == 0){
                        // Put error into key that means that user with such name already exist
                        error = 1;
                        // Put 1 at the key as a flag of finish process for LoginActivity.java
                        answerState = 1;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (Exception e){
                System.out.println("Exp=" + e);
            }
            answerState = 1;
            return null;
        }
    }
}