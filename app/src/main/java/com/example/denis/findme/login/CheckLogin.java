package com.example.denis.findme.login;

/**
 * Class used for check login.
 */
public final class CheckLogin {

    private String login;
    private int errorNumber = 0;
    private static final int MIN_LOGIN_LENGTH = 4;
    private static final int MAX_LOGIN_LENGTH = 15;
    // Errors
    private static final int MIN_LENGHT_ERROR = 1;
    private static final int MAX_LENGHT_ERROR = 2;
    private static final int RUSSIAN_LETTERS_ERROR = 3;

    /**
     * Constructor.
     * @param login
     */
    public CheckLogin(String login){
        this.login = login;
    }

    /**
     * Method for checking min length for login.
     * if length less then MIN_LOGIN_LENGTH, set "1" in errorNumber field (it is key for checking
     * state in future). if length more then MAX_LOGIN_LENGTH, set "2" in errorNumber field
     *
     */
    private void checkLength(){
        if (login.length() < MIN_LOGIN_LENGTH){
            errorNumber = MIN_LENGHT_ERROR;
        } else if (login.length() > MAX_LOGIN_LENGTH){
            errorNumber = MAX_LENGHT_ERROR;
        }
    }

    /**
     * Method for all situations check.
     * if something wrong with user login set key in error number field.
     */
    public void mainCheck(){
        if (errorNumber == 0){
            checkLength();
        }
        if (errorNumber == 0){
            checkLanguage();
        }
    }


    /**
     * Method for checking language of login. We should set "3" in in errorNumber field (it is key for checking
     * state in future) if login contains russian symbols (it is wrong for database).
     */
    private void checkLanguage(){
        final String russianAlphabet = "ёйцукенгшщзхъфывапролджэячсмитьбю";
        login = login.toLowerCase();
        // Key for checking russian letters existing
        int russianLettestKey = 0;
        char loginArray[] = login.toCharArray();
        for (int i = 0; i < login.length(); i++){
            if (russianAlphabet.indexOf(loginArray[i]) != -1){
                russianLettestKey = 1;
                break;
            }
        }
        if (russianLettestKey != 0){
            errorNumber = RUSSIAN_LETTERS_ERROR;
        }
    }

    /**
     * Method for getting error number.
     * "1" is error in length (less then min length).
     * "2" is error in length (more then max length).
     * "3" is error in russian symbols.
     * @return
     * int value.
     */
    public int getErrorNumber(){
        return  errorNumber;
    }

}
