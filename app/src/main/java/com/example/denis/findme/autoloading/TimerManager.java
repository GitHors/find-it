package com.example.denis.findme.autoloading;

import com.example.denis.findme.sendinfo.SendCoordinates;

/**
 * TimerManager class used for organize work of SendCoordinates class with NetworkChangeReceiver class.
 * Because of multichange of network state we can have problem with timer in SendCoordinates class
 * (for avoid this problem we create this class, which does not create new object of SendCoordinates
 * while old object timer does not damage).
 *
 * @see SendCoordinates
 * @see NetworkChangeReceiver
 */
public class TimerManager {

    public static SendCoordinates mySend;

    /**
     * Constructor for
     * @param send
     */
    TimerManager(SendCoordinates send){
        if (mySend == null){
            mySend = send;
        }
    }

    /**
     * Constructor for recreate SendCoordinate object in NetworkChangeReceiver method after
     * second right connection for internet (after first turning of internet).
     * @param send
     * @param k
     */
    TimerManager(SendCoordinates send, int k){
            mySend = send;
    }

    /**
     * Method for start timer for SendCoordinate object
     */
    public static void start(){
        mySend.startTimer();
    }

    /**
     * Method for cancel timer for SendCoordinate object
     */
    public static void stop(){
        mySend.stopTimer();
    }
}
