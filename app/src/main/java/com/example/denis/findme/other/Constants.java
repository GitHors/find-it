package com.example.denis.findme.other;

/**
 * Class constants used for saving constants.
 */
public final class Constants {

    public static final String REGISTRATION_PHP_FILE_URL = "http://justfindit.ml/php/reg.php";
    public static final String UPDATE_INFO_PHP_FILE_URL = "http://justfindit.ml/php/update_info.php";
    public static final String GET_INFO_PHP_FILE_URL = "http://justfindit.ml/php/get_info.php";

    // Name of fields in database
    public static final String DATABASE_ID_USER_FIELD = "id_tel_user";
    public static final String DATABASE_PASSWORD_USER_FIELD = "password";
    public static final String DATABASE_SEND_COORD_FIELD = "is_send_coord";
    public static final String DATABASE_BLOCK_FIELD = "is_block";
    public static final String DATABASE_LATITUDE_FIELD = "latitude";
    public static final String DATABASE_LONGTITUDE_FIELD = "longtitude";
    public static final String DATABASE_DATA_FIELD = "data";
    // Default values for database
    public static final String DATABASE_DEFAULT_VALUE = "0";

    // Database fields for shared preference (name of parameters in shared preference)
    public static final String DATABASE_USER_ID_VALUE = "user_id";
    public static final String DATABASE_PASSWORD_VALUE = "user_password";


    // Toast messages for check login
    public static final String MIN_LOGIN_LENGTH_TOAST = "Длина логина должна быть более 4 символов";
    public static final String MAX_LOGIN_LENGTH_TOAST = "Длина логина должна быть менее 15 символов";
    public static final String LOGIN_RUSSIAN_SYMBOLS_TOAST = "Логин не должен содержать русских символов";
    public static final String SUCCESS = "Успешно";

    // Toast messages for check password
    public static final String MIN_PASSWORD_LENGTH_TOAST = "Длина пароля должна быть более 4 символов";
    public static final String MAX_PASSWORD_LENGTH_TOAST = "Длина пароля должна быть менее 15 символов";
    public static final String PASSWORD_RUSSIAN_SYMBOLS_TOAST = "Пароль не должен содержать русских символов";
    public static final String PASSWORD_NUMBERS_SYMBOLS_TOAST = "Пароль должен содержать хотя бы одну цифру";

    // Errors
    public static final int MIN_LENGHT_ERROR = 1;
    public static final int MAX_LENGHT_ERROR = 2;
    public static final int NUMBERS_NOT_EXISTING_ERROR = 3;
    public static final int LOGIN_RUSSIAN_LETTERS_ERROR = 3;
    public static final int RUSSIAN_LETTERS_ERROR = 4;

    // For first start shared preference
    public static final String FIRST_START_KEY = "first_start";

    // Error for checking internet connection
    public static final String ERROR_INTERNET_CONNECTION = "Проверьте подключение к интернету";

    // Refresh info button text
    public static final String REFRESH_BUTTON = "Обновите";
    public static final String REFRESH_BUTTON_SIMPLE_STATE = "Сохранено";

    // First sending info
    public static int FIRST_CONNECTION = 1;


}
