package com.example.denis.findme;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.denis.findme.database.Registartion;
import com.example.denis.findme.login.CheckLogin;
import com.example.denis.findme.login.CheckPassword;
import com.example.denis.findme.other.Constants;

/**
 * LoginActivity class.
 */
public class LoginActivity extends Activity  {

    private static final String LOGIN_ERROR_HINT = "Новый логин";
    private static final String PASSWORD_HINT = "Введите пароль";
    Button registrationButton, cancelButton;
    EditText login, password;

    private CheckLogin checkLogin;
    private CheckPassword checkPassword;

    private SharedPreferences sharedPreferences;
    private SharedPreferences loginPreferences;
    private CheckPreference checkPreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Constants.FIRST_CONNECTION = 0;

        // Shared preference for saving user id and password
        sharedPreferences = getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE);
        loginPreferences = getSharedPreferences("loginpr", Context.MODE_PRIVATE);
        checkPreference = new CheckPreference();

        // Navigation buttons
        registrationButton = (Button) findViewById(R.id.registration);
        cancelButton = (Button) findViewById(R.id.cancel);

        // Input fields for login and password
        login = (EditText) findViewById(R.id.loginInput);
        password = (EditText) findViewById(R.id.passwordInput);

        login.setHint(LOGIN_ERROR_HINT);
        password.setHint(PASSWORD_HINT);
    }

    public void OnClickRegistrationButton(View view) {

        if (isInternetConnectionExist()){
            String userLogin = login.getText().toString();
            String userPassword = password.getText().toString();
            checkLogin = new CheckLogin(userLogin);
            checkPassword = new CheckPassword(userPassword);

            // Checking input data
            checkLogin.mainCheck();
            checkPassword.mainCheck();

            Toast toast = Toast.makeText(getApplicationContext(),
                    checkErrorForToast(), Toast.LENGTH_SHORT);
            if (checkErrorForToast().equals(Constants.SUCCESS)){

                checkPreference.setPreferenceValue(loginPreferences, Constants.DATABASE_USER_ID_VALUE, userLogin);
                checkPreference.setPreferenceValue(loginPreferences, Constants.DATABASE_PASSWORD_VALUE, userPassword);

                new Registartion(userLogin, userPassword);
                while (Registartion.answerState != 1){
                    SystemClock.sleep(500);
                }
                if (Registartion.error == 1){
                    toast = Toast.makeText(getApplicationContext(),
                            "User already exist", Toast.LENGTH_SHORT);
                } else {
                    // Set key for first start (user get success registration)
                    sharedPreferences = getSharedPreferences(MainActivity.APP_SETTINGS, Context.MODE_PRIVATE);
                    checkPreference.setPreferenceValue(sharedPreferences, Constants.FIRST_START_KEY, "1");
                    SystemClock.sleep(200);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            }

            toast.show();
//            SystemClock.sleep(500);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(),
                    Constants.ERROR_INTERNET_CONNECTION, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void OnClickCancelButton(View view) {
        finish();
    }

    /**
     * Method for getting phone number (in future will be used as login).
     * If there is error in getting number put hint in editText and say user that he should
     * this number himself.
     * @return
     * String value.
     * ("+375291769450" or LOGIN_ERROR_HINT)
     */
    private String getPhoneNumber(){
        String result;
        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        result = tMgr.getLine1Number();
        if (result.equals("")){
            return LOGIN_ERROR_HINT;
        } else {
            return result;
        }
    }

    /**
     * Method for set message into toast.
     * We check login and password for errors and put message if they are exist, else put "success" message
     * @return
     * String value.
     */
    private String checkErrorForToast(){
        String toastText = Constants.SUCCESS;
        if (checkLogin.getErrorNumber() == Constants.MIN_LENGHT_ERROR){
            toastText = Constants.MIN_LOGIN_LENGTH_TOAST;
        } else if (checkLogin.getErrorNumber() == Constants.MAX_LENGHT_ERROR){
            toastText = Constants.MAX_LOGIN_LENGTH_TOAST;
        } else if (checkLogin.getErrorNumber() == Constants.LOGIN_RUSSIAN_LETTERS_ERROR){
            toastText = Constants.LOGIN_RUSSIAN_SYMBOLS_TOAST;
        } else if (checkPassword.getErrorNumber() == Constants.MIN_LENGHT_ERROR){
            toastText = Constants.MIN_PASSWORD_LENGTH_TOAST;
        } else if (checkPassword.getErrorNumber() == Constants.MAX_LENGHT_ERROR){
            toastText = Constants.MAX_PASSWORD_LENGTH_TOAST;
        } else if (checkPassword.getErrorNumber() == Constants.NUMBERS_NOT_EXISTING_ERROR){
            toastText = Constants.PASSWORD_NUMBERS_SYMBOLS_TOAST;
        } else if (checkPassword.getErrorNumber() == Constants.RUSSIAN_LETTERS_ERROR){
            toastText = Constants.PASSWORD_RUSSIAN_SYMBOLS_TOAST;
        }
        return  toastText;
    }

    /**
     * Method for check internet connection.
     * @return
     * true if phome has internet connection, false if not.
     */
    private boolean isInternetConnectionExist() {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

}




