package com.example.denis.findme;

import android.content.SharedPreferences;
import android.widget.TextView;

import com.example.denis.findme.other.Constants;

/**
 * Class used for manipulate value of parameter in sharedPreference by given parameters.
 * Also we put info about sharedPreference in TextView.
 */
public class CheckPreference {

    private static final String ON_STATE = "On";
    private static final String OFF_STATE = "Off";
    private static final String ON_PREFERENCE_STATUS = "1";

    /**
     * Constructor.
     */
    public CheckPreference(){

    }

    /**
     * Method for put info in sharedPreference.
     * @param sharedPreferences
     * @param keyName
     * name of parameter for writing (String value).
     * @param keyValue
     * value of parameter (String value).
     */
    public void setPreferenceValue (SharedPreferences sharedPreferences, String keyName, String keyValue){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(keyName, keyValue);
        editor.commit();
    }

    /**
     * Method for put info about sharedPreference in TextView.
     * "On" - if sgaredPreference contains "1".
     * "Off" - if sgaredPreference contains "0".
     * @param textView
     * TextView value.
     * @param sharedPreferences
     * sharedPreference for gettin key value.
     * @param keyName
     * String value.
     */
    public void setTextAboutPreferState(TextView textView, SharedPreferences sharedPreferences, String keyName){
        if (sharedPreferences.getString(keyName, "").equals(ON_PREFERENCE_STATUS)){
            textView.setText(ON_STATE);
        } else {
            textView.setText(OFF_STATE);
        }
    }

    /**
     * Method for put info about refresh state in TextView.
     * @param textView
     * TextView value.
     * @param sharedPreferences
     * sharedPreference for gettin key value.
     * @param keyName
     * String value.
     */
    public void setTextAboutRefreshState(TextView textView, SharedPreferences sharedPreferences, String keyName){
        if (!sharedPreferences.getString(keyName, "").equals(ON_PREFERENCE_STATUS)){
            textView.setText(Constants.REFRESH_BUTTON_SIMPLE_STATE);
        } else {
            textView.setText(Constants.REFRESH_BUTTON);
        }
    }

}
