package com.example.denis.findme.sendinfo;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.example.denis.findme.LoginActivity;
import com.example.denis.findme.MainActivity;
import com.example.denis.findme.database.GetInfo;
import com.example.denis.findme.database.Registartion;
import com.example.denis.findme.database.UpdateInfo;
import com.example.denis.findme.other.Constants;
import com.example.denis.findme.other.GetSharedPreference;
import com.example.denis.findme.other.MyApplication;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


/**
 * SendCoordinates class used for organize work with server. Produce turn on/off timer for sending
 * JSON objects with (latitude, longitude, lastConnection) to the server.
 */
public class SendCoordinates extends Activity{

    private static final String FIRST_SEND_AFTER_TURN_ON_NETWORK_CONNECTION = "connection";

    // Fields for create JSON Object (coordinates and data)
    private double latitude;
    private double longitude;
    private Date lastConnection;

    // Name of settings file
    public static final String APP_SETTINGS = "coordinates";


    // Timer for working with internet connection and sending info to the server
    Timer timer;

    // Other fields
    Context context;
    LocationManager locationManager;

    SharedPreferences loginPrefernce;


    /**
     * Constructor.
     */
    public SendCoordinates(Context context){
        this.context = context;
        timer = new Timer();
        loginPrefernce = context.getSharedPreferences("loginpr", Context.MODE_PRIVATE);
    }

    /**
     * Method stopTimer used for cancel timer.
     * Cancel work after turn off connection with internet.
     *
     * @see com.example.denis.findme.autoloading.TimerManager
     */
    public void stopTimer(){
        if (timer != null){
            timer.cancel();
        }
    }


    /**
     * Method startTimer used for turn on timer.
     * Start work after turn on connection with internet.
     *
     * @see com.example.denis.findme.autoloading.TimerManager
     */
    public void startTimer(){
        if (Constants.FIRST_CONNECTION == 1) {


            Date timerPeriod = new Date();
//            timerPeriod.setTime((seekbar.getProgress()) * 1000);
//            Log.d("timer", (seekbar.getProgress()) * 1000 + " seconds");
//            timer.schedule(task, 0, (seekbar.getProgress()) * 1000);
            timer = new Timer();

            timerPeriod.setTime(15000);
            Log.d("timer", (5 * 1000 + " seconds"));
            timer.schedule(task, 0, 15 * 1000);
        }
    }

    /**
     * LocationListener used for work with finding phone coordinates.
     */
    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            formatLocation(location);
        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @TargetApi(Build.VERSION_CODES.M)
        @Override
        public void onProviderEnabled(String provider) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (provider.equals(LocationManager.NETWORK_PROVIDER)) {

            }
        }
    };

    /**
     * Method formatLocation used for filling fields about phone coordinates and last date of connection.
     * @param location
     */
    private void formatLocation(Location location) {
        if (location != null){
            latitude = location.getLatitude();
            longitude = location.getLongitude();
            lastConnection = new Date(location.getTime());
        }
    }

    /**
     * TimerTask for timer. Used for sending information to the server (in JSON format).
     */
    public TimerTask task = new TimerTask() {
        @Override
        public void run() {


                locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                }
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 0, 0,
                        locationListener, Looper.getMainLooper());
                JSONObject locationInfo = new JSONObject();
                try {
                    locationInfo.put("latitude", latitude);
                    locationInfo.put("longitude", longitude);
                    locationInfo.put("date", lastConnection);
                    String json = locationInfo.toString();
                    Log.d("json", json);
                    Log.d("json", "Info about location = " + locationInfo.toString());


                    String login = loginPrefernce.getString(Constants.DATABASE_USER_ID_VALUE, "");
                    String password = loginPrefernce.getString(Constants.DATABASE_PASSWORD_VALUE, "");

                    new GetInfo(login, password).execute();
                    while (GetInfo.answerState != 1){
                        Log.d("Check 1", "...");
                        SystemClock.sleep(500);
                    }
                    if (GetInfo.sendCoordinates.equals("1")){
                        Log.d("Check 4", "...");
                        new UpdateInfo(login, password, "1", "1", String.valueOf(latitude), String.valueOf(longitude),
                                String.valueOf(lastConnection)).execute();
                    }

                    //new RequestTask(locationInfo).execute("http://forgsy.cf/lastlocation.php");
                } catch (JSONException e) {
                    Log.d("json", "Error in create JSON object");
                }
            }
    };


}
