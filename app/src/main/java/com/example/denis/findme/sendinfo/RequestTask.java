package com.example.denis.findme.sendinfo;

import android.os.AsyncTask;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Class RequestTask used for sending information from JSON object to server.
 */
public class RequestTask extends AsyncTask<String, String, String> {

    private JSONObject sendInfo;

    /**
     * Constructor.
     */
    public RequestTask(JSONObject locationInfo){
        this.sendInfo = locationInfo;
    }

    /**
     * Method to organize work with server.
     * @param params
     * String value.
     * @return String value.
     */
    @Override
    public String doInBackground(String... params) {
        try {
            //Create query to the server
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            //He will be send post query
            HttpPost postMethod = new HttpPost(params[0]);
            //Send one parameter (JSONObject)
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
            nameValuePairs.add(new BasicNameValuePair("info", sendInfo.toString()));
            //Sending to the serer
            postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            String response = hc.execute(postMethod, res);
            Log.d("Success", "Send info");
        } catch (Exception e) {
            System.out.println("Exp=" + e);
        }
        return null;
    }
}
