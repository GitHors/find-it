package com.example.denis.findme;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

public class LoadingActivity extends AppCompatActivity {

    // Name of settings file
    public static final String APP_SETTINGS = "coordinates";
    SharedPreferences sharedPreferences;
    private static final String FIRST_START_KEY = "first_start";
    private static final String REGISTRATION_EXIST_KEY = "1";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        sharedPreferences = getSharedPreferences(APP_SETTINGS, Context.MODE_PRIVATE);

        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    if (sharedPreferences.getString(FIRST_START_KEY, "").equals(REGISTRATION_EXIST_KEY)){
                        Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(LoadingActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}
