package com.example.denis.findme.login;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for class CheckPassword.
 */
public class CheckPasswordTest {

    @Test
    public void  testMinLenght() {
        final int expectedResult = 1;
        CheckPassword checkPassword = new CheckPassword("qwe");
        checkPassword.mainCheck();
        assertEquals("Error in checking min length", expectedResult,checkPassword.getErrorNumber());
    }

    @Test
    public void  testMaxLenght() {
        final int expectedResult = 2;
        CheckPassword checkPassword = new CheckPassword("0123456789876543210");
        checkPassword.mainCheck();
        assertEquals("Error in checking max length", expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testRightLenght() {
        final int expectedResult = 0;
        CheckPassword checkPassword = new CheckPassword("qwert123");
        checkPassword.mainCheck();
        assertEquals("Error in checking right password", expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testLowercaseRussianSymbols() {
        final int expectedResult = 4;
        CheckPassword checkPassword = new CheckPassword("дt12ybc");
        checkPassword.mainCheck();
        assertEquals("Error in checking lowercase russian letters", expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testUppercaseRussianSymbols() {
        final int expectedResult = 4;
        CheckPassword checkPassword = new CheckPassword("ДtЕb12c");
        checkPassword.mainCheck();
        assertEquals("Error in checking uppercase russian letters", expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testRussianSymbols() {
        final int expectedResult = 4;
        CheckPassword checkPassword = new CheckPassword("1ывыааыв");
        checkPassword.mainCheck();
        assertEquals("Error in checking russian letters", expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testMinLengthRussianSymbols() {
        final int expectedResult = 1;
        CheckPassword checkPassword = new CheckPassword("ывы");
        checkPassword.mainCheck();
        assertEquals("Error ",
                expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testMaxLengthRussianSymbols() {
        final int expectedResult = 2;
        CheckPassword checkPassword = new CheckPassword("ывsвывыаыаыаыаыаыаыа564564654ы");
        checkPassword.mainCheck();
        assertEquals("Error in checking russian letters (max length should get error)",
                expectedResult, checkPassword.getErrorNumber());
    }

    @Test
    public void  testNumberExisting() {
        final int expectedResult = 3;
        CheckPassword checkPassword = new CheckPassword("qwert");
        checkPassword.mainCheck();
        assertEquals("Error in checking numbers in password",
                expectedResult, checkPassword.getErrorNumber());
    }


}