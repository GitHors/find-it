package com.example.denis.findme.login;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests for class CheckLogin.
 */
public class CheckLoginTest {

    @Test
    public void  testMinLenght() {
        final int expectedResult = 1;
        CheckLogin checkLogin = new CheckLogin("den");
        checkLogin.mainCheck();
        assertEquals("Error in checking min length", expectedResult,checkLogin.getErrorNumber());
    }

    @Test
    public void  testMaxLenght() {
        final int expectedResult = 2;
        CheckLogin checkLogin = new CheckLogin("0123456789876543210");
        checkLogin.mainCheck();
        assertEquals("Error in checking max length", expectedResult, checkLogin.getErrorNumber());
    }

    @Test
    public void  testRightLenght() {
        final int expectedResult = 0;
        CheckLogin checkLogin = new CheckLogin("+375291769450");
        checkLogin.mainCheck();
        assertEquals("Error in checking right login length", expectedResult, checkLogin.getErrorNumber());
    }

    @Test
    public void  testLowercaseRussianSymbols() {
        final int expectedResult = 3;
        CheckLogin checkLogin = new CheckLogin("дtybc");
        checkLogin.mainCheck();
        assertEquals("Error in checking lowercase russian letters", expectedResult, checkLogin.getErrorNumber());
    }

    @Test
    public void  testUppercaseRussianSymbols() {
        final int expectedResult = 3;
        CheckLogin checkLogin = new CheckLogin("ДtЕbc");
        checkLogin.mainCheck();
        assertEquals("Error in checking uppercase russian letters", expectedResult, checkLogin.getErrorNumber());
    }

    @Test
    public void  testRussianSymbols() {
        final int expectedResult = 3;
        CheckLogin checkLogin = new CheckLogin("ывыааыв");
        checkLogin.mainCheck();
        assertEquals("Error in checking russian letters", expectedResult, checkLogin.getErrorNumber());
    }

    @Test
    public void  testMinLengthRussianSymbols() {
        final int expectedResult = 1;
        CheckLogin checkLogin = new CheckLogin("ывы");
        checkLogin.mainCheck();
        assertEquals("Error in checking russian letters (min length should get error)",
                expectedResult, checkLogin.getErrorNumber());
    }

    @Test
    public void  testMaxLengthRussianSymbols() {
        final int expectedResult = 2;
        CheckLogin checkLogin = new CheckLogin("ывsвывыаыаыаыаыаыаыа564564654ы");
        checkLogin.mainCheck();
        assertEquals("Error in checking russian letters (max length should get error)",
                expectedResult, checkLogin.getErrorNumber());
    }


}